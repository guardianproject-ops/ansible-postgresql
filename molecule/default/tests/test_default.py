import os
import yaml
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.fixture(scope="module")
def AnsibleVars(host):
    stream = host.file("/tmp/ansible-vars.yml").content
    return yaml.safe_load(stream)


@pytest.mark.parametrize(
    "name,content",
    [
        ("/etc/postgresql/11/main/pg_hba.conf", "host    postgres        all             192.168.93.0/24         ident"),
        ("/etc/postgresql/11/main/conf.d/25ansible_postgresql.conf", "max_connections = 250"),
        ("/etc/postgresql/11/main/conf.d/25ansible_postgresql.conf", "work_mem = '8MB'")
    ],
)
def test_files(host, name, content):
    f = host.file(name)
    assert f.exists
    if content:
        assert f.contains(content)


@pytest.mark.parametrize("name", ["postgresql"])
def test_services(host, name):
    service = host.service(name)
    assert service.is_running
    assert service.is_enabled

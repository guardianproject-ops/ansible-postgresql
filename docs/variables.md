## Role Variables

* `postgresql_version`: `11` - the postgres debian package version to install



* `postgresql_conf`: `{}` - A dictionary of `postgresql.conf` options and values. These options are not added to `postgresql.conf` directly - the role adds a `conf.d` subdirectory in the configuration directory and an include statement for that directory to `postgresql.conf`. Options set in `postgresql_conf` are then set in `conf.d/25ansible_postgresql.conf`. Due to YAML parsing, you must take care when defining values in `postgresql_conf` to ensure they are properly written to the config file. For



* `postgresql_pg_hba_conf`: `[]` - A list of lines to add to `pg_hba.conf`



* `postgresql_pg_hba_local_postgres_user`: `true` - If set to `false`, this will remove the `postgres` user's entry from `pg_hba.conf` that is preconfigured on Debian-based PostgreSQL installations. You probably do not want to do this unless you know what you're doing.



* `postgresql_pg_hba_local_socket`: `true` - If set to `false`, this will remove the `local` entry from `pg_hba.conf` that is preconfigured by the PostgreSQL package.



* `postgresql_pg_hba_local_ipv4`: `true` - If set to `false`, this will remove the `host ... 127.0.0.1/32` entry from `pg_hba.conf` that is preconfigured by the PostgreSQL package.



* `postgresql_pg_hba_local_ipv6`: `true` - If set to `false`, this will remove the `host ... ::1/128` entry from `pg_hba.conf` that is preconfigured by the PostgreSQL


